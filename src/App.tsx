import React from 'react';
import { ThemeProvider } from 'styled-components';
import logo from './logo.svg';
import theme from './styles/theme';
import { GlobalStyle } from './styles/global.style';
import Exchanger from './components/organisms/exchange/Exchanger';

function App() {
    return (
        <ThemeProvider theme={theme}>
            <GlobalStyle />
            <div className='app'>
                <header>
                    <img src={logo} className='app-logo' alt='logo' />
                </header>
                <div role='main'>
                    <div className='content'>
                        <Exchanger />
                    </div>
                </div>
            </div>
        </ThemeProvider>
    );
}

export default App;
