/** Select Option Types */
export interface OptionType {
    value: string;
    label: string;
    isDisabled?: boolean;
    isFixed?: boolean;
    priority?: 1 | 2 | 3 | 4 | 5;
    showViewAction?: boolean;
}

/**
 * This type is used to style components content according to color scheme
 */
export type Kind = 'default' | 'primary' | 'secondary' | 'success' | 'warning' | 'danger' | 'info';

/**
 * Type used to size components in relation to theme
 */
export type Size = 'small' | 'medium' | 'large';
