/* eslint-disable no-param-reassign */
export const groupByKey = (key: string, array: object[]) =>
    array.reduce((objectsByKeyValue, obj) => {
        const value = obj[key];
        objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
        return objectsByKeyValue;
    }, {} as { [key: string]: object[] });
