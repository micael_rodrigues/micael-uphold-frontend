import { useState, useCallback, useRef, useEffect } from 'react';

/**
 * Debounce function delays the processing of the function for a amount of time in order to improve code performance
 * The goal is to reduce overhead by preventing a function from being called several times in succession.
 *
 * Example:
 * const [debouncedCallback] = useDebouncedCallback(value => { *Code bo te executed* }, 600);
 *
 * useEffect(() => { debouncedCallback(value); }, [*property that will trigger debounce*]); // trigger that will call the debounce function
 *
 * @template T
 * @param {T} callback
 * @param {number} wait (ms)
 * @returns {[T]}
 */
export const useDebouncedFunction = <T extends (...args) => void>(fn: T, wait: number): [T, () => void] => {
    const debouncedFn = fn;
    const fnTimeOutHandler = useRef(null);

    const cancelDebouncedFunction: () => void = useCallback(() => {
        clearTimeout(fnTimeOutHandler.current);
        fnTimeOutHandler.current = null;
    }, []);

    const debouncedFunction = useCallback(
        (...args) => {
            cancelDebouncedFunction();
            fnTimeOutHandler.current = setTimeout(() => {
                debouncedFn(...args);
            }, wait);
        },
        [cancelDebouncedFunction, debouncedFn, wait],
    );

    return [debouncedFunction as T, cancelDebouncedFunction];
};

/**
 * Debounce value delays the processing of the value change for a amount of time in order to improve code performance
 * The goal is to reduce overhead by preventing the value change action from being called several times in succession.
 *
 * Example:
 * const [value, setValue] = useState(field.value);
 * const [text] = useDebouncedValue<string>(*Property that will be listen*, 1000);
 *
 * useEffect(() => { *Code to be executed* }, [text]); //when text changes (debounced value) effect can be called
 *
 * @template T
 * @param {T} value
 * @param {number} wait
 * @returns {[T]}
 */
export const useDebouncedValue = <T>(value: T, wait: number): [T, () => void] => {
    const [state, dispatch] = useState(value);
    const [debouncedFunction, cancel] = useDebouncedFunction(
        useCallback((newValue) => dispatch(newValue), []),
        wait,
    );
    const previousValue = useRef(value);

    useEffect(() => {
        // prevent running debounce callback on first render
        if (previousValue.current !== value) {
            debouncedFunction(value);
            previousValue.current = value;
        }
    }, [value, debouncedFunction]);

    return [state, cancel];
};
