import { useRef, useState } from 'react';

export interface LongPressHandlers {
    onMouseDown: () => void;
    onMouseUp: () => void;
    onMouseLeave: () => void;
    onTouchStart: () => void;
    onTouchEnd: () => void;
}

/**
 * This hook is used to execute a callback function while mouse button is pressed
 * it stops when user leave the element or releases the mouse button
 *
 * @param {*} callback
 * @param {number} [ms=300]
 * @returns
 */
const useLongPress = (callback, endCallback, ms = 300) => {
    const [initialDelay] = useState(ms);
    const timerId = useRef(null);
    const msRef = useRef(ms);

    /**
     * Execute the callback function and register the next execution
     * The next execution time will be shorten in relation to the time user keeps mouse pressed
     */
    const repeat = () => {
        callback();
        timerId.current = setTimeout(repeat, msRef.current);
        msRef.current /= 2;
    };

    /**
     * Start the repeat callback action
     */
    const start = () => {
        repeat();
    };

    /**
     * Stops the execution clearing the next call
     */
    const stop = () => {
        if (!timerId.current) {
            return;
        }
        msRef.current = initialDelay;
        clearTimeout(timerId.current);
        timerId.current = null;
        endCallback();
    };

    return {
        handlers: {
            onMouseDown: start,
            onMouseUp: stop,
            onMouseLeave: stop,
            onTouchStart: start,
            onTouchEnd: stop,
        } as LongPressHandlers,
        stop,
    };
};

export default useLongPress;
