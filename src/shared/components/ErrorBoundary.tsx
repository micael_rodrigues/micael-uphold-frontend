import { Component, ReactNode } from 'react';

// the clock's state has one field: The current time, based upon the
// JavaScript class Date
type ErrorState = {
    hasError: boolean;
    error: any;
};

type ErrorProps = {
    fallback: ReactNode;
};

class ErrorBoundary extends Component<ErrorProps, ErrorState> {
    constructor(props) {
        super(props);
        // eslint-disable-next-line react/no-unused-state
        this.state = { hasError: false, error: null };
    }

    static getDerivedStateFromError(e) {
        return {
            hasError: true,
            error: e,
        } as ErrorState;
    }

    render() {
        if (this.state.hasError) {
            return this.props.fallback;
        }
        return this.props.children;
    }
}

export default ErrorBoundary;
