import colors from './colors.scss';
import typography from './fonts.scss';

type Colors = typeof colors;
type Fonts = typeof typography;

declare module 'styled-components' {
    export interface DefaultTheme {
        colors: Colors;
        typography: Fonts;
    }
}

const theme = {
    colors,
    typography,
};

export { colors, typography };
export default theme;
