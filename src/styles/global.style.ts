import { createGlobalStyle } from 'styled-components';
import { typography } from './theme';

export const GlobalStyle = createGlobalStyle`
  html, body {
    margin: 0;
    padding: 0;
    height: 100vh;
    font-family: ${typography.fontFamily};
  }
  *, *::after, *::before {
    box-sizing: border-box;
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New', monospace;
  }

  header {
    background: ${({ theme }) => theme.colors.secondaryDark5};
    box-shadow: 0 2px 5px rgba(0, 0, 0, 0.16);
    /* stretch from first grid line to last grid line */
    grid-column: 1/-1;
    -ms-grid-column-span: 2;
    grid-row: 1;
  }

  .app {
    height: 100%;
    display: grid;
    grid-template-rows: 3.75rem 1fr;
    grid-template-columns: 1fr;
    text-align: center;
  }

  .app-logo {
    height: 80%;
    pointer-events: none;
    padding-top: 0.5rem;
   }

  [role='main'] {
    margin: 1rem;
    grid-row: 2;
    grid-column: 1;
  }

  .content {
      margin: auto;
      max-width: 1200px;
  }
  
`;
