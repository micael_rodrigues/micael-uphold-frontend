import React, {
    lazy,
    useState,
    useRef,
    useCallback,
    Suspense,
    unstable_SuspenseList as SuspenseList,
    unstable_useTransition as useTransition,
} from 'react';
import { fetchTickerData } from '../../../api/Api';
import Grid from '../../atoms/grid/Grid';
import { ValueType } from '../../atoms/select/Select';
import Loader from '../../molecules/loader/Loader';
import ErrorBoundary from '../../../shared/components/ErrorBoundary';
import Alert from '../../molecules/alert/Alert';

const ExchangeRates = lazy(() => import('./ExchangeRates'));
const ExchangeForm = lazy(() => import('./ExchangesForm'));

const Exchanger = () => {
    const initialCurrency = localStorage.getItem('lastCurrency') || 'USD';
    const initialValue = +localStorage.getItem('lastValue') || 1;

    const [resource, setResource] = useState(() => fetchTickerData(initialCurrency));
    const currencyRef = useRef(initialCurrency);
    const [value, setValue] = useState<number>(initialValue); // Start at one

    const [startTransition, isPending] = useTransition();

    const handleValueChange = useCallback((stepperValue: number) => {
        setValue(stepperValue || 0);
        localStorage.setItem('lastValue', `${stepperValue || 0}`);
    }, []);

    const handleCurrencyChange = useCallback(
        (options: ValueType) => {
            currencyRef.current = options[0].value;
            localStorage.setItem('lastCurrency', currencyRef.current);
            startTransition(() => {
                setResource(fetchTickerData(currencyRef.current));
            });
        },
        [startTransition],
    );

    return (
        <SuspenseList revealOrder='forwards' tail='hidden'>
            <Suspense fallback={<Loader kind='primary' message='Loading Form...' />}>
                <ExchangeForm
                    resource={resource}
                    currency={currencyRef.current}
                    {...{ value, handleCurrencyChange, handleValueChange, isPending }}
                />
            </Suspense>
            <Suspense fallback={<Loader kind='primary' message='Loading Rates...' />}>
                <Grid columns='1fr 1fr' gap='0.5rem' columnsSM='1fr' style={{ paddingTop: '0.5rem' }}>
                    <ErrorBoundary
                        key={currencyRef.current}
                        fallback={
                            <Alert
                                kind='danger'
                                message={`Something when wrong while loading data for ${currencyRef.current}`}
                            />
                        }
                    >
                        <ExchangeRates {...{ resource, value }} />
                    </ErrorBoundary>
                </Grid>
            </Suspense>
        </SuspenseList>
    );
};

export default Exchanger;
