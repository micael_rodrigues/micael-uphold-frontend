import React from 'react';
import Text from '../../atoms/text/Text';
import Box from '../../atoms/box/Box';
import Currency from '../../molecules/currency/Currency';
import Alert from '../../molecules/alert/Alert';

interface ExchangeRatesProps {
    resource: any;
    value: number;
}

const ExchangeRates = ({ resource, value }: ExchangeRatesProps) => {
    const exchangeRates = resource.exchangeRates.read();

    if (!exchangeRates || !exchangeRates.length) {
        return <Alert message='No data available for this currency' />;
    }

    return (
        <>
            {exchangeRates.map((e) => (
                <Box key={e.pair} justify='space-between' showBorder>
                    <Text animated>{`${e.ask * value}`}</Text>
                    <Currency iso3={e.currency} />
                </Box>
            ))}
        </>
    );
};

export default ExchangeRates;
