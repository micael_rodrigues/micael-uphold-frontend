import React from 'react';
import { render, RenderResult, waitForElement, fireEvent } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import { mocked } from 'ts-jest/utils';
import Exchanger from '../Exchanger';
import theme from '../../../../styles/theme';
import { fetchTickerData } from '../../../../api/Api';

let component: RenderResult;

// Mock API
jest.mock('../../../../api/Api', () => {
    return {
        fetchTickerData: jest.fn().mockImplementation(() => {
            return {
                currencies: {
                    read() {
                        return ['USD', 'EUR', 'BTC'];
                    },
                },
                exchangeRates: {
                    read() {
                        return [
                            {
                                ask: '0.88705',
                                bid: '0.88705',
                                currency: 'EUR',
                                pair: 'USDEUR',
                            },
                            {
                                ask: '0.00010377',
                                bid: '0.00010354',
                                currency: 'BTC',
                                pair: 'USDBTC',
                            },
                        ];
                    },
                },
            };
        }),
    };
});

describe('Exchanger app tests', () => {
    const MockedApi = mocked(fetchTickerData, true);

    beforeEach(async () => {
        MockedApi.mockClear();
        component = render(
            <ThemeProvider theme={theme}>
                <Exchanger />
            </ThemeProvider>,
        );
    });

    it('renders async and matches snapshot', async () => {
        const lazyElement = await waitForElement(() => component.getByText('USD'));
        expect(lazyElement).toBeInTheDocument();
        expect(component).toMatchSnapshot();
    });

    it('calls fetch ticker data on first render', () => {
        expect(MockedApi).toHaveBeenCalledTimes(1);
    });

    it('has defined currencies and rates in document', () => {
        const data = ['USD', 'EUR', 'BTC', '0.88705', '0.00010377'];
        for (const c of data) {
            expect(component.getByText(c)).toBeInTheDocument();
        }
    });

    it('updates exchange rates when changing value', async () => {
        const newValue = 3;
        expect.assertions(2);
        fireEvent.change(component.getByTestId('stepper-input'), {
            target: { value: newValue },
        });

        const expectedValues = [`${0.88705 * newValue}`, `${0.00010377 * newValue}`];

        for (const v of expectedValues) {
            // eslint-disable-next-line no-await-in-loop
            expect(await component.findByText(v)).toBeInTheDocument();
        }
    });
});
