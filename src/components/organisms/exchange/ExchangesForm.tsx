import React, { ReactNode, useMemo } from 'react';
import Grid from '../../atoms/grid/Grid';
import Stepper from '../../atoms/stepper/Stepper';
import Select, { ValueType, OptionsType } from '../../atoms/select/Select';
import Currency from '../../molecules/currency/Currency';
import { OptionType } from '../../../shared/types/Types';

const DummyCurrency = { value: 'DUMMY', label: 'DUMMY' };

const currencyAsOption = (option: OptionType): ReactNode => {
    return <Currency iso3={option.value} iconPosition='left' />;
};

interface ExchangesFormProps {
    isPending: boolean;
    currency: string;
    value: number;
    handleValueChange: (stepperValue: number) => void;
    handleCurrencyChange: (options: ValueType) => void;
    resource: any;
}

const ExchangesForm = ({
    value,
    currency,
    handleValueChange,
    handleCurrencyChange,
    isPending,
    resource,
}: ExchangesFormProps) => {
    const currencies = useMemo<OptionsType>(() => {
        const arr = resource.currencies.read().map((c) => ({ value: c, label: c }));
        arr.push(DummyCurrency);
        return arr;
    }, [resource.currencies]);

    return (
        <Grid columns='1fr 1fr' gap='0.5rem' columnsSM='1fr'>
            <Stepper
                sizeType='large'
                isDisabled={isPending}
                min={0}
                max={1000000000000000000000}
                step={0.1}
                placeholder='1.22355'
                initialValue={value}
                handleOnChange={handleValueChange}
            />
            <Select
                size='large'
                isDisabled={isPending}
                options={currencies}
                formatOption={currencyAsOption}
                onChange={handleCurrencyChange}
                placeholder='Select a currency'
                defaultValue={currency}
            />
        </Grid>
    );
};

export default ExchangesForm;
