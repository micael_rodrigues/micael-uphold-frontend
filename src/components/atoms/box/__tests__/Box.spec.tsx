import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import Box from '../Box';

let component: RenderResult;
const BoxContent = () => (
    <>
        <div className='test'>Hello</div>
        <div className='test'>Hello</div>
    </>
);

describe('Box components tests', () => {
    beforeEach(() => {
        component = render(
            <Box>
                <BoxContent />
            </Box>,
        );
    });

    it('base properties were not changed', () => {
        expect(Box.defaultProps.align).toBe('center');
        expect(Box.defaultProps.direction).toBe('row');
        expect(Box.defaultProps.justify).toBe('flex-start');
    });

    it('renders correctly', () => {
        expect(component).toMatchSnapshot();
    });

    it('has two children', () => {
        expect(component.getAllByText('Hello')).toHaveLength(2);
    });
});
