import React, { ReactNode } from 'react';
import BoxStyled, { BoxStyledProps } from './Box.style';

export interface BoxProps extends BoxStyledProps {
    children: ReactNode;
}

const Box = ({ children, align, direction, justify, showBorder }: BoxProps) => (
    <BoxStyled {...{ align, direction, justify, showBorder }}>{children}</BoxStyled>
);

Box.defaultProps = {
    align: 'center',
    direction: 'row',
    justify: 'flex-start',
};

export default Box;
