import styled, { css } from 'styled-components';

export interface BoxStyledProps {
    /** Vertical alignment for the content */
    align?: 'flex-start' | 'center' | 'flex-end' | 'stretch';
    /** Flex vertical content arrangement */
    direction?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
    /** Flex horizontal content arrangement */
    justify?: 'flex-start' | 'flex-end' | 'center' | 'space-around' | 'space-between' | 'space-evenly';
    /** Show border or not */
    showBorder?: boolean;
}

const BorderStyle = css`
    box-shadow: 0px 1px 5px 0px rgba(0, 0, 0, 0.2), 0px 2px 2px 0px rgba(0, 0, 0, 0.14),
        0px 3px 1px -2px rgba(0, 0, 0, 0.12);
    border-radius: 2px;
`;

const BoxStyled = styled.div<BoxStyledProps>`
    display: flex;
    padding: 0.5rem;
    width: 100%;
    height: auto;
    display: flex;
    flex-wrap: wrap;
    ${({ showBorder }) => (showBorder ? BorderStyle : null)}
    align-items: ${({ align }) => align};
    justify-content: ${({ justify }) => justify};
    flex-direction: ${({ direction }) => direction};
`;

export default BoxStyled;
