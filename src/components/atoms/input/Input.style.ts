import styled, { css } from 'styled-components';
import { Size, Kind } from '../../../shared/types/Types';

interface IInputProps {
    kind?: Kind;
    sizeType?: Size;
    isDisabled: boolean;
}

const sizes = {
    small: css`
        padding: 0px 7px;
        font-size: ${({ theme }) => theme.typography.fontSM};
    `,
    large: css`
        padding: 0px 11px;
        font-size: ${({ theme }) => theme.typography.fontLG};
    `,
    medium: css`
        padding: 0px 11px;
        font-size: ${({ theme }) => theme.typography.fontMD};
    `,
};

export const InputGroupStyled = styled.div<IInputProps>`
    display: flex;
    align-items: center;
    flex-wrap: nowrap;
    flex-grow: 1;
    background-color: ${({ theme }) => theme.colors.white};
    border-radius: 4px;
    border: 1px solid ${({ theme, kind }) => (kind ? theme.colors[kind] : theme.colors.greyLight35)};
    line-height: 1.5;
    ${(props) => sizes[props.sizeType]};
    ${(props) =>
        props.isDisabled
            ? css`
                  cursor: not-allowed;
                  background-color: ${({ theme }) => theme.colors.disabled};
              `
            : null};
`;

export const InputStyled = styled.input<IInputProps>`
    box-sizing: border-box;
    margin: 0 !important;
    padding: 0;
    font-variant: tabular-nums;
    list-style: none;
    font-feature-settings: 'tnum';
    position: relative;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    display: flex;
    flex-grow: 1;
    height: 100%;
    width: 100%;
    color: ${({ theme }) => theme.colors.text};
    background-color: ${({ theme }) => theme.colors.white};
    background-image: none;
    border: none !important;
    transition: all 0.3s;
    cursor: text;

    :focus {
        outline: 0;
    }

    :disabled {
        cursor: not-allowed;
        background-color: ${({ theme }) => theme.colors.disabled};
    }

    ::placeholder {
        color: ${({ theme }) => theme.colors.textPlaceholder};
    }

    ::-webkit-clear-button {
        display: none;
    }

    ::-ms-clear {
        display: none;
    }

    ::-webkit-inner-spin-button,
    ::-webkit-outer-spin-button {
        -webkit-appearance: none;
        appearance: none;
        margin: 0;
    }
`;

export const PrefixStyled = styled.span`
    display: flex;
    align-items: center;
    width: 20px;
    height: 100%;
    margin-right: 3px;
`;

export const SuffixStyled = styled.span`
    display: flex;
    align-items: center;
    width: 20px;
    height: 100%;
    margin-left: 3px;
`;
