import React, { ReactNode, SyntheticEvent, useCallback } from 'react';
import { InputGroupStyled, InputStyled, PrefixStyled, SuffixStyled } from './Input.style';
import { Size } from '../../../shared/types/Types';

export interface InputProps {
    /** Set the input disabled */
    isDisabled?: boolean;
    /** Set Error Indication */
    inError?: boolean;
    /** Initial value of input */
    initialValue?: string | number;
    /**
     * Can be set to primary, secondary, success, warning, danger or omitted (meaning default)
     * this will affect the input border color
     */
    kind?: 'primary' | 'secondary' | 'success' | 'warning' | 'danger' | 'default';
    /** Maximum characters allowed on input */
    maxLength?: number;
    /** Input name */
    name?: string;
    /** Event occurs when input loses focus */
    onBlur?: (event: SyntheticEvent) => void;
    /** Event occurs when input value has been changed */
    onChange?: (event: SyntheticEvent) => void;
    /** Event occurs when input gets focus */
    onFocus?: (event: SyntheticEvent) => void;
    /** Event occurs when user is pressing a key */
    onKeyDown?: (event: SyntheticEvent) => void;
    /** Event occurs when user releases a key */
    onKeyUp?: (event: SyntheticEvent) => void;
    /** Event occurs when Enter key is pressed */
    onPressEnter?: (event: SyntheticEvent) => void;
    /** Specifies a short hint that describes the expected value in the input field */
    placeholder?: string;
    /** Prefix content of input */
    prefix?: ReactNode;
    /** Set input to readonly mode */
    readOnly?: boolean;
    /** Set the size of input, can be set to small, default or large omitted (meaning default)  */
    sizeType?: Size;
    /** Step attribute used in number and time type */
    step?: number;
    /** Suffix content of input */
    suffix?: ReactNode;
    /** The type of input */
    type: string;
}

const Input = ({
    isDisabled,
    initialValue,
    kind,
    placeholder,
    prefix,
    readOnly,
    sizeType,
    suffix,
    type,
    onChange,
    ...remaining
}: InputProps) => {
    const handleOnChange = useCallback(
        (event) => {
            event.persist();
            onChange(event);
        },
        [onChange],
    );

    return (
        <InputGroupStyled {...{ isDisabled, kind, sizeType }}>
            {prefix && <PrefixStyled>{prefix}</PrefixStyled>}
            <InputStyled
                // eslint-disable-next-line react/jsx-props-no-spreading
                {...{ ...remaining, disabled: isDisabled }}
                sizeType={sizeType}
                isDisabled={isDisabled}
                autoComplete='off'
                onChange={!isDisabled ? handleOnChange : undefined}
                placeholder={placeholder}
                value={initialValue}
                type={type}
                readOnly={readOnly}
            />
            {suffix && <SuffixStyled>{suffix}</SuffixStyled>}
        </InputGroupStyled>
    );
};

Input.defaultProps = {
    initialValue: '',
    onChange: () => undefined,
    readOnly: false,
    sizeType: 'medium',
};

export default Input;
