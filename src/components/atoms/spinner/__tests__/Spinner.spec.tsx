import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import Spinner from '../Spinner';

let component: RenderResult;

describe('Spinner components tests', () => {
    beforeEach(() => {
        component = render(<Spinner />);
    });

    it('base properties were not changed', () => {
        expect(Spinner.defaultProps.kind).toBe('default');
        expect(Spinner.defaultProps.speed).toBe(0.5);
    });

    it('renders correctly', () => {
        expect(component).toMatchSnapshot();
    });
});
