import React, { ReactNode, useCallback, useEffect, useRef, useState } from 'react';
import Components from './components/Components';
import SelectStyled, { SelectStyles } from './Select.style';
import { OptionType, Kind, Size } from '../../../shared/types/Types';

export type OptionsType = OptionType[];

export type ValueType = OptionType | OptionsType | null | undefined;

export interface SelectProps {
    /** List of options */
    options: OptionsType;
    /** Default selected value(s) */
    defaultValue?: string;
    /** Format option  */
    formatOption?: (option: OptionType) => ReactNode;
    /** Define a fixed size for the select, it will override the size property */
    height?: string;
    /** Disables select picker */
    isDisabled?: boolean;
    /**
     * Can be set to primary, secondary, success, warning, danger or omitted (meaning default)
     * this will affect the select border color
     */
    kind?: Kind;
    /** Maximum characters allowed on search input */
    maxLength?: number;
    /** Change event */
    onChange?: (value: ValueType) => void;
    /** Select placeholder */
    placeholder?: string;
    /** Select size */
    size?: Size;
}

/**
 * Get selected {OptionsType}
 * @param options object
 * @param value to find
 * @returns {OptionsType}
 */
export const getSelectedOption = (options: OptionsType, value: string | undefined): OptionsType => {
    if (!value) {
        return [];
    }

    return [options.find((o) => value === o.value)];
};

const Select = (props: SelectProps) => {
    const { defaultValue, options, formatOption, isDisabled, maxLength, onChange } = props;
    const isRendering = useRef(true);
    const [value, setValue] = useState<OptionsType>(getSelectedOption(options, defaultValue));
    const [disabled, setDisabled] = useState(isDisabled);

    useEffect(() => {
        setDisabled(isDisabled);
    }, [isDisabled]);

    useEffect(() => {
        // It's first render, do not trigger change callback
        if (!isRendering.current && onChange) {
            onChange(value);
        } else {
            isRendering.current = false;
        }
    }, [onChange, value]);

    const handleOnChange = async (newValue: ValueType): Promise<void> => {
        let updatedValue = newValue;

        if (!Array.isArray(updatedValue) && updatedValue !== null) {
            updatedValue = [updatedValue];
        } else if (!Array.isArray(updatedValue)) {
            updatedValue = [];
        }

        setValue(updatedValue);
    };

    const formatOptionLabel = useCallback(
        (option: OptionType): ReactNode => {
            if (formatOption) {
                return formatOption(option);
            }
            return <>{option.label}</>;
        },
        [formatOption],
    );

    const SearchInput = useCallback(
        (inputProps: any) => <Components.SearchInput inputProps={inputProps} maxLength={maxLength} />,
        [maxLength],
    );

    const selectProps = {
        ...props,
        className: 'uphold',
        options,
        value,
        isDisabled: disabled,
        hideIndicatorSeparator: true,
        isClearable: false,
        hideSelectedOptions: true,
        components: {
            Input: SearchInput,
            IndicatorSeparator: null,
        },
        formatOptionLabel,
        onChange: handleOnChange,
        styles: SelectStyles({
            size: props.size,
            kind: props.kind,
            fixedHeight: props.height,
            isDisabled,
        }),
    };
    return <SelectStyled {...selectProps} />;
};

Select.defaultProps = {
    isDisabled: false,
    kind: 'default',
    maxLength: 100,
    placeholder: '',
    size: 'medium',
};

export default Select;
