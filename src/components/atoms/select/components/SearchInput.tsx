import React from 'react';
import { components } from 'react-select';

export interface SearchInputProps {
    /** Maximum characters allowed on input */
    maxLength?: number;
    /** React-select input props (should be {@typedef InputProps} but it does not support extension) */
    inputProps: any;
}

const SearchInput = ({ maxLength, inputProps }: SearchInputProps) => {
    return <components.Input {...inputProps} maxLength={maxLength} />;
};

export default SearchInput;
