import Select from 'react-select';
import styled from 'styled-components';
import { colors, typography } from '../../../styles/theme';

const SelectStyled = styled(Select)``;

const containerSize = {
    small: '20px',
    large: '45px',
    medium: '30px',
};

const loadingIndicatorSize = {
    small: '12px',
    large: '35px',
    medium: '18px',
};

const fontSize = {
    small: typography.fontSM,
    large: typography.fontLG,
    medium: typography.fontMD,
};

const kindColor = {
    primary: colors.primary,
    secondary: colors.secondary,
    success: colors.success,
    warning: colors.warning,
    danger: colors.danger,
    default: colors.greyLight35,
};

const getOptionColor = (state) => {
    if (state.isDisabled) {
        return colors.textDisabled;
    }
    if (state.isSelected) {
        return colors.primary;
    }
    if (state.isFocused) {
        return colors.primaryLight15;
    }
    return colors.text;
};

const getOptionCursor = (state) => {
    if (state.isDisabled) {
        return 'not-allowed';
    }
    if (state.isFocused) {
        return 'pointer';
    }
    return 'auto';
};

const getValueColor = (state: any, selectIsDisabled: boolean) => {
    const { isFixed } = state.data;
    if (isFixed) {
        return colors.white;
    }

    if (selectIsDisabled) {
        return colors.text;
    }
    return getOptionColor(state);
};

const getBackgroundColor = (state) => {
    if (state.isSelected || state.isFocused) {
        return !state.isDisabled ? colors.secondary : colors.white;
    }
    return colors.white;
};

const getFontWeight = ({ data }) => {
    return data.priority > 0 ? 'bold' : 'inherit';
};

/**
 * Object with custom react-select styles
 */
export const SelectStyles = (props) => {
    return {
        container: (provided, state) => ({
            ...provided,
            fontFamily: typography.fontFamily,
            fontSize: fontSize[props.size],
            color: colors.text,
            width: '100%',
            cursor: state.isDisabled ? 'not-allowed' : 'auto',
            pointerEvents: 'all',
        }),
        control: (provided, state) => ({
            ...provided,
            outline: state.isFocused ? colors.primaryLight15 : colors.text,
            borderColor: `${kindColor[props.kind]}!important`,
            paddingLeft: '0.6875rem',
            flexWrap: 'nowrap',
            boxShadow: 'none',
            borderRadius: '0.25rem',
            minHeight: props.fixedHeight ? props.fixedHeight : containerSize[props.size],
            width: '100%',
            pointerEvents: state.isDisabled ? 'none' : 'auto',
            backgroundColor: state.isDisabled ? colors.disabled : colors.white,
        }),
        valueContainer: (provided, state) => ({
            ...provided,
            height: '100%',
            overflow: state.isMulti ? 'auto' : 'hidden',
            flexWrap: state.isMulti ? 'wrap' : 'nowrap',
            padding: 0,
            alignContent: props.fixedHeight ? 'baseline' : 'center',
        }),
        placeholder: (provided, state) => ({
            ...provided,
            position: 'relative',
            top: props.fixedHeight ? '6%' : '35%',
            color: colors.textPlaceholder,
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            transform: 'none',
            display: state.isFocused ? 'none' : 'auto',
        }),
        singleValue: (provided, state) => ({
            ...provided,
            width: '100%',
            div: {
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
            },
            color: getValueColor(state, props.isDisabled),
            fontWeight: getFontWeight(state),
        }),
        multiValue: (provided, state) => ({
            ...provided,
            backgroundColor: state.data.isFixed ? colors.grey : colors.greyLight35,
            border: props.isDisabled ? `1px solid ${colors.greyLight15}` : 'initial',
        }),
        multiValueLabel: (provided, state) => {
            const { isFixed, showViewAction } = state.data;
            const hasView = props.showViewAction && showViewAction !== false;

            return {
                ...provided,
                paddingRight: isFixed || (props.isDisabled && !hasView) ? '6px' : '2px',
                color: getValueColor(state, props.isDisabled),
                fontWeight: getFontWeight(state),
            };
        },
        multiValueRemove: (provided, state) => ({
            ...provided,
            color: colors.grey,
            display: state.data.isFixed || props.isDisabled ? 'none' : 'flex',
            ':hover': {
                cursor: 'pointer',
                color: colors.text,
            },
        }),
        indicatorsContainer: (provided) => ({
            ...provided,
            alignItems: props.fixedHeight ? 'baseline' : 'center',
            div: {
                padding: '0 3px 0 0',
                color: colors.greyLight35,
                ':hover': {
                    cursor: 'pointer',
                    color: colors.text,
                },
                display: 'flex',
                justifyContent: 'center',
                svg: {
                    width: loadingIndicatorSize[props.size],
                    path: {
                        fill: `${kindColor[props.kind]}`,
                    },
                    ':hover': {
                        path: {
                            fill: kindColor.primary,
                        },
                    },
                },
            },
        }),
        menu: (provided) => ({
            ...provided,
            fontFamily: typography.fontFamily,
            fontSize: fontSize[props.size],
            marginTop: 1,
            boxShadow: '0 2px 8px rgba(0, 0, 0, 0.15)',
            borderRadius: '0.25rem',
        }),
        menuList: (provided) => ({
            ...provided,
            padding: 0,
            borderRadius: '0.25rem',
        }),
        menuPortal: (provided) => ({
            ...provided,
            zIndex: 102,
        }),
        loadingMessage: (provided) => {
            return {
                ...provided,
                div: {
                    display: 'flex',
                    justifyContent: 'center',
                    svg: {
                        width: loadingIndicatorSize[props.size],
                    },
                },
            };
        },
        option: (provided, state) => {
            return {
                ...provided,
                backgroundColor: getBackgroundColor(state),
                color: getOptionColor(state),
                padding: '2px 12px',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                whiteSpace: 'nowrap',
                cursor: getOptionCursor(state),
                fontWeight: getFontWeight(state),
                ':active': {
                    backgroundColor: colors.white,
                },
            };
        },
    };
};

export default SelectStyled;
