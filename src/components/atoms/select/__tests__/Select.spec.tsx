import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import Select from '../Select';

let component: RenderResult;
let handleChange: () => void;
const Currencies = [
    {
        value: 'USD',
        label: 'USD',
    },
    {
        value: 'EUR',
        label: 'EUR',
    },
    {
        value: 'XPT',
        label: 'XPT',
    },
];

describe('Select components tests', () => {
    beforeEach(() => {
        handleChange = jest.fn(() => undefined);
        component = render(<Select options={Currencies} defaultValue='USD' onChange={handleChange} />);
    });

    it('base properties were not changed', () => {
        expect(Select.defaultProps.isDisabled).toBe(false);
        expect(Select.defaultProps.maxLength).toBe(100);
        expect(Select.defaultProps.kind).toBe('default');
        expect(Select.defaultProps.placeholder).toBe('');
        expect(Select.defaultProps.size).toBe('medium');
    });

    it('renders correctly', () => {
        expect(component).toMatchSnapshot();
    });

    it('Default value is present', () => {
        expect(component.getByText('USD')).toBeInTheDocument();
    });

    it('Change was not called on first render', () => {
        expect(handleChange).toHaveBeenCalledTimes(0);
    });
});
