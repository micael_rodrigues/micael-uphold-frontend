import React, { ReactNode, SyntheticEvent, useEffect, useState } from 'react';
import { useDebouncedValue } from '../../../shared/hooks/useDebounce';
import useLongPress from '../../../shared/hooks/useLongPress';
import StepperButtons from './components/StepperButtons';
import { MinusStyled, PlusStyled, StepperInputStyled, StepperStyled } from './Stepper.style';
import { Kind, Size } from '../../../shared/types/Types';

interface IStepperProps {
    /** Increment and Decrement input value buttons placement */
    buttonPosition?: 'left' | 'right' | 'both';
    /** Handler for event that occurs when input value has been changed */
    handleOnChange?: (value: number) => void;
    /** Initial numeric value */
    initialValue?: number;
    /** Set the stepper disabled */
    isDisabled?: boolean;
    /**
     * Can be set to primary, secondary, success, warning, danger or omitted (meaning default)
     * this will affect the spinner border color
     */
    kind?: Kind;
    /** Min value allowed on input */
    min?: number;
    /** Max value allowed on input */
    max?: number;
    /** Handler for event that occurs when stepper loses focus */
    onBlur?: (event: SyntheticEvent) => void;
    /** Specifies a short hint that describes the expected value in the numeric input field */
    placeholder?: string;
    /**
     * An integer specifying the number of significant digits.
     * By default is omitted and component will calculate it using the step value
     */
    precision?: number;
    /** Set the size of input, can be set to small, medium, large or omitted (meaning medium)  */
    sizeType?: Size;
    /** Value that will be increased or decreased in the current value when user click on buttons */
    step?: number;
    /** Indicator that stepper is validating */
    validating?: ReactNode;
}

const Stepper = ({
    buttonPosition,
    handleOnChange,
    initialValue,
    isDisabled,
    kind,
    min,
    max,
    onBlur,
    placeholder,
    precision,
    sizeType,
    step,
    validating,
}: IStepperProps) => {
    const [value, setValue] = useState(initialValue);
    const [minReached, setMinReached] = useState(false);
    const [maxReached, setMaxReached] = useState(false);
    const [numValue] = useDebouncedValue<number>(value, 600);

    useEffect(() => {
        handleOnChange(numValue);
    }, [numValue, handleOnChange]);

    const countDecimals = () => {
        if (step % 1 !== 0) {
            return step.toString().split('.')[1].length;
        }
        return 0;
    };

    const [numberPrecision] = useState(precision || countDecimals());

    const handleDecrement = () => {
        if (isDisabled) {
            return;
        }

        if (min === undefined || value === undefined || value > min) {
            setValue((v) => Number(((v || 0) - step).toFixed(numberPrecision)));
        }
    };

    const handleIncrement = () => {
        if (isDisabled) {
            return;
        }

        if (max === undefined || value === undefined || value < max) {
            setValue((v) => Number(((v || 0) + step).toFixed(numberPrecision)));
        }
    };

    const handleStopChange = () => {
        if (isDisabled) {
            return;
        }

        handleOnChange(value);
    };

    const handleOnStepChange = (event) => {
        if (event.target.value !== '') {
            setValue(Number(event.target.value));
        } else {
            setValue(min || undefined);
        }
    };

    const decrement = useLongPress(handleDecrement, handleStopChange);
    const increment = useLongPress(handleIncrement, handleStopChange);

    useEffect(() => {
        if (min !== undefined) {
            if (value <= min) {
                setMinReached(true);
                setValue(min);
                decrement.stop();
                return;
            }
            setMinReached(false);
        }

        if (max !== undefined) {
            if (value >= max) {
                setMaxReached(true);
                setValue(max);
                increment.stop();
            } else {
                setMaxReached(false);
            }
        }
    }, [decrement, increment, max, min, value]);

    return (
        <StepperStyled
            sizeType={sizeType}
            kind={kind}
            buttonPosition={buttonPosition}
            isDisabled={isDisabled}
            onBlur={onBlur}
        >
            {buttonPosition !== 'both' && (
                <StepperButtons
                    isDisabled={isDisabled}
                    position={buttonPosition}
                    sizeType={sizeType}
                    incrementHandlers={increment.handlers}
                    maxReached={maxReached}
                    decrementHandlers={decrement.handlers}
                    minReached={minReached}
                />
            )}

            {buttonPosition === 'both' && (
                // eslint-disable-next-line react/jsx-props-no-spreading
                <MinusStyled btn='minusbtn' {...decrement.handlers} minReached={minReached}>
                    -
                </MinusStyled>
            )}
            <StepperInputStyled
                data-testid='stepper-input'
                isDisabled={isDisabled}
                autoComplete='off'
                onChange={handleOnStepChange}
                placeholder={placeholder}
                initialValue={value === undefined ? '' : value}
                type='number'
                step={step}
                min={min}
                max={max}
                sizeType={sizeType}
                buttonPosition={buttonPosition}
                prefix={buttonPosition === 'left' ? validating : null}
                suffix={buttonPosition !== 'left' ? validating : null}
            />
            {buttonPosition === 'both' && (
                // eslint-disable-next-line react/jsx-props-no-spreading
                <PlusStyled btn='plusbtn' {...increment.handlers} maxReached={maxReached}>
                    +
                </PlusStyled>
            )}
        </StepperStyled>
    );
};

Stepper.defaultProps = {
    buttonPosition: 'right',
    handleOnChange: () => undefined,
    kind: 'default',
    placeholder: '',
    sizeType: 'medium',
    step: 1,
};

export default Stepper;
