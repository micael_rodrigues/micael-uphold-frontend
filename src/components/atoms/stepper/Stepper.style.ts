/* eslint-disable @typescript-eslint/no-explicit-any */
import styled, { css } from 'styled-components';
import { colors, typography } from '../../../styles/theme';
import Input from '../input/Input';
import { Size } from '../../../shared/types/Types';

const kindColor = {
    primary: colors.primary,
    secondary: colors.secondary,
    success: colors.success,
    warning: colors.warning,
    danger: colors.danger,
    default: colors.greyLight35,
};

const sizes = {
    small: '20px',
    medium: '30px',
    large: '45px',
};

const fontSize = {
    small: typography.fontSM,
    medium: typography.fontMD,
    large: typography.fontLG,
};

const paddingSize = {
    small: 0,
    medium: '0 8px',
    large: 0,
};

export const StepperStyled = styled.div<any>`
    font-family: ${typography.fontFamily};
    font-size: ${(props) => fontSize[props.sizeType]};
    display: flex;
    flex-direction: ${(props) => (props.buttonPosition === 'right' ? 'row-reverse' : 'row')};
    height: ${(props) => sizes[props.sizeType]};
    border: 1px solid ${(props) => kindColor[props.kind]};
    border-radius: 0.25rem;
    line-height: 1.5;
    background-color: ${(props) => (props.isDisabled ? colors.disabled : undefined)};
    flex-grow: 1;

    :hover {
        div {
            opacity: 1;
            transition: opacity 0.5s;
        }
    }

    > div {
        border: none;
        margin-left: 1px;
    }
`;

export const StepperInputStyled = styled(Input)<any>`
    font-family: ${typography.fontFamily};
    padding: ${(props) => paddingSize[props.sizeType]};
    ${(props) =>
        props.buttonPosition === 'left'
            ? css`
                  text-align: right;
              `
            : null};

    -moz-appearance: textfield;

    ::-webkit-inner-spin-button,
    ::-webkit-outer-spin-button {
        -webkit-appearance: none;
        appearance: none;
        margin: 0;
    }
`;

const StepperButton = styled.button`
    display: flex;
    align-items: center;
    justify-content: center;
    border: none;
    background-image: none;
    text-shadow: none;
    box-shadow: none;
    background-color: ${colors.white};

    :focus {
        outline: 0;
    }

    :hover {
        background-color: ${colors.secondaryDark5};
    }

    :disabled {
        opacity: 0.5;
    }

    :hover:disabled {
        cursor: not-allowed;
    }
`;

export const MinusStyled = styled(StepperButton)<any>`
    border-radius: 4px 0 0 4px;
    border-right: 1px solid ${colors.lightGrey};

    :hover {
        cursor: ${(props) => (props.minReached ? 'not-allowed' : 'pointer')};
    }
`;

export const PlusStyled = styled(StepperButton)<any>`
    border-radius: 0 4px 4px 0;
    border-left: 1px solid ${colors.lightGrey};

    :hover {
        cursor: ${(props) => (props.maxReached ? 'not-allowed' : 'pointer')};
    }
`;

type VerticalButtonsStyle = {
    sizeType: Size;
};

export const VerticalButtonsStyle = styled.div<VerticalButtonsStyle>`
    display: flex;
    flex-direction: column;
    width: ${(props) => sizes[props.sizeType]};
    border-radius: 0 3px 3px 0;
    opacity: 0;
    transition: opacity 0.5s;
    span {
        color: ${colors.default};
    }
`;

export const UpButtonStyled = styled(StepperButton)<any>`
    display: flex;
    justify-items: center;
    height: 50%;
    border-bottom: 1px solid ${colors.default};
    padding: 0;

    ${(props) =>
        props.position === 'right'
            ? css`
                  border-radius: 0 3px 0 0;
                  border-left: 1px solid ${colors.default};
              `
            : css`
                  border-radius: 3px 0 0 0;
                  border-right: 1px solid ${colors.default};
              `};

    :hover {
        span {
            color: ${colors.text};
        }
        cursor: ${(props) => (props.maxReached ? 'not-allowed' : 'pointer')};
    }
`;

export const DownButtonStyled = styled(StepperButton)<any>`
    height: 50%;
    padding: 0;

    ${(props) =>
        props.position === 'right'
            ? css`
                  border-radius: 0 0 3px 0;
                  border-left: 1px solid ${colors.default};
              `
            : css`
                  border-radius: 0 0 0 3px;
                  border-right: 1px solid ${colors.default};
              `};

    :hover {
        span {
            color: ${colors.text};
        }
        cursor: ${(props) => (props.minReached ? 'not-allowed' : 'pointer')};
    }
`;
