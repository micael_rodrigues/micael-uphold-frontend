/* eslint-disable react/jsx-props-no-spreading */
import React, { memo } from 'react';
import { DownButtonStyled, UpButtonStyled, VerticalButtonsStyle } from '../Stepper.style';
import { LongPressHandlers } from '../../../../shared/hooks/useLongPress';
import Arrow from './Arrow.style';

interface StepperButtonsProps {
    /** Set the stepper disabled */
    isDisabled?: boolean;
    /** Increment and Decrement input value buttons placement */
    position?: 'left' | 'right' | 'both';
    /**
     * An integer specifying the number of significant digits.
     * By default is omitted and component will calculate it using the step value
     */
    precision?: number;
    /** Set the size of input, can be set to small, medium, large or omitted (meaning medium)  */
    sizeType?: 'small' | 'medium' | 'large';
    // if maximum has been reached
    maxReached: boolean;
    // if minimum has been reached
    minReached: boolean;
    // Increment handlers from useLongPress hook
    incrementHandlers?: LongPressHandlers;
    // Decrement handlers from useLongPress hook
    decrementHandlers?: LongPressHandlers;
}
const StepperButtons = memo(
    ({
        isDisabled,
        position,
        maxReached,
        minReached,
        sizeType,
        incrementHandlers,
        decrementHandlers,
    }: StepperButtonsProps) => (
        <VerticalButtonsStyle sizeType={sizeType}>
            <UpButtonStyled
                btn='incrementbtn'
                disabled={isDisabled}
                {...incrementHandlers}
                maxReached={maxReached}
                position={position}
                type='button'
            >
                <Arrow type='up' />
            </UpButtonStyled>
            <DownButtonStyled
                btn='decrementbtn'
                disabled={isDisabled}
                {...decrementHandlers}
                minReached={minReached}
                position={position}
                type='button'
            >
                <Arrow type='down' />
            </DownButtonStyled>
        </VerticalButtonsStyle>
    ),
);

export default StepperButtons;
