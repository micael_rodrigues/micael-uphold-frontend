import styled, { css } from 'styled-components';

type ArrowProps = {
    type: 'up' | 'down';
};

const types = {
    up: css`
        transform: rotate(-135deg);
    `,
    down: css`
        transform: rotate(45deg);
    `,
};

const Arrow = styled.i<ArrowProps>`
    border: dashed ${(props) => props.theme.colors.primary};
    border-width: 0 0.15rem 0.15rem 0;
    display: inline-block;
    padding: 0.15rem;
    ${(props) => types[props.type]}
`;

export default Arrow;
