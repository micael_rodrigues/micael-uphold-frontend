import React from 'react';
import { render, fireEvent, RenderResult } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import Stepper from '../Stepper';
import theme from '../../../../styles/theme';

let component: RenderResult;
let handleChange: (value: number) => void;

describe('Stepper components tests', () => {
    beforeEach(() => {
        handleChange = jest.fn();
        component = render(
            <ThemeProvider theme={theme}>
                <Stepper min={0} max={2} initialValue={1} handleOnChange={handleChange} />
            </ThemeProvider>,
        );
    });

    it('base properties were not changed', () => {
        expect(Stepper.defaultProps.kind).toBe('default');
        expect(Stepper.defaultProps.buttonPosition).toBe('right');
        expect(Stepper.defaultProps.placeholder).toBe('');
        expect(Stepper.defaultProps.sizeType).toBe('medium');
        expect(Stepper.defaultProps.step).toBe(1);
    });

    it('renders', () => {
        expect(component).toMatchSnapshot();
    });

    it('increments value until maximum and stops', () => {
        const upButton = component.getAllByRole('button')[0];
        fireEvent.click(upButton);
        fireEvent.click(upButton); // second click cannot trigger change
        expect(handleChange).toHaveBeenCalledTimes(1);
        /* const input = component.getByTestId('stepper-input');
        console.log(input);
        expect(input).toHaveValue(2);
        */
    });

    it('decrements value until minimum and stops', () => {
        const downButton = component.getAllByRole('button')[1];
        fireEvent.click(downButton);
        fireEvent.click(downButton); // second click cannot trigger change
        expect(handleChange).toHaveBeenCalledTimes(1);
    });
});
