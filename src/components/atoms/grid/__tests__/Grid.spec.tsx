import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import Grid from '../Grid';

let component: RenderResult;
const GridContent = () => (
    <>
        <div className='test'>Hello</div>
        <div className='test'>Hello</div>
        <div className='test'>Hello</div>
        <div className='test'>Hello</div>
    </>
);

describe('Grid components tests', () => {
    beforeEach(() => {
        component = render(
            <Grid>
                <GridContent />
            </Grid>,
        );
    });

    it('base properties were not changed', () => {
        expect(Grid.defaultProps.align).toBe('unset');
        expect(Grid.defaultProps.columns).toBe('none');
        expect(Grid.defaultProps.gap).toBe('0');
        expect(Grid.defaultProps.justify).toBe('unset');
        expect(Grid.defaultProps.rows).toBe('none');
        expect(Grid.defaultProps.flow).toBe('row');
    });

    it('renders correctly', () => {
        expect(component).toMatchSnapshot();
    });

    it('has four children', () => {
        expect(component.getAllByText('Hello')).toHaveLength(4);
    });
});
