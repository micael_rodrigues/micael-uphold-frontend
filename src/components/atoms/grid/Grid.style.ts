import styled, { css } from 'styled-components';
import { HTMLAttributes } from 'react';

export interface IGridStyledProps extends HTMLAttributes<HTMLDivElement> {
    /** Vertical alignment for the grid cells' content */
    align?: 'start' | 'center' | 'end' | 'stretch';
    /** Value for grid-template-areas css property */
    areas?: string;
    /** When screen size <600px you can define the areas. If value is not defined, the default will be the value defined in areasSM */
    areasXS?: string;
    /** When screen size <900px you can define the areas. If value is not defined, the default will be the value defined in areasMD */
    areasSM?: string;
    /** When screen size <1200px you can define the areas. If value is not defined, the default will be the value defined in areasLG */
    areasMD?: string;
    /** When screen size <1800px you can define the areas. If value is not defined, the default will be the value defined in areasXL */
    areasLG?: string;
    /** When screen size >=1800px you can define the areas. If value is not defined, the default will be the value defined in areas */
    areasXL?: string;
    /** grid-template-columns property of CSS  */
    columns?: string;
    /** When screen size <600px you can define the columns property. If value is not defined, the default will be the value defined in columnsSM */
    columnsXS?: string;
    /** When screen size <900px you can define the columns property. If value is not defined, the default will be the value defined in columnsMD */
    columnsSM?: string;
    /** When screen size <1200px you can define the columns property. If value is not defined, the default will be the value defined in columnsLG */
    columnsMD?: string;
    /** When screen size <1800px you can define the columns property. If value is not defined, the default will be the value defined in columnsXL */
    columnsLG?: string;
    /** When screen size >=1800px you can define the columns property. If value is not defined, the default will be the value defined in columns */
    columnsXL?: string;
    /** grid-auto-flow property of CSS */
    flow?: 'row' | 'column' | 'dense' | 'row dense' | 'column dense';
    /** Gap size */
    gap?: string;
    /** Horizontal grid cells' content arrangement */
    justify?: 'start' | 'end' | 'center';
    /** Number of rows */
    rows?: string;
    /** When screen size <600px you can define the rows property. If value is not defined, the default will be the value defined in rowsSM */
    rowsXS?: string;
    /** When screen size <900px you can define the rows property. If value is not defined, the default will be the value defined in rowsMD */
    rowsSM?: string;
    /** When screen size <1200px you can define the rows property. If value is not defined, the default will be the value defined in rowsLG */
    rowsMD?: string;
    /** When screen size <1800px you can define the rows property. If value is not defined, the default will be the value defined in rowsXL */
    rowsLG?: string;
    /** When screen size >=1800px you can define the rows property. If value is not defined, the default will be the value defined in rows */
    rowsXL?: string;
}

enum WidthScale {
    XS = 'xs',
    SM = 'sm',
    MD = 'md',
    LG = 'lg',
    XL = 'xl',
}
/** Areas */
const getAreas = (props: IGridStyledProps) => (props.areas ? props.areas : 'none');
const getAreasXL = (props: IGridStyledProps) => (props.areasXL ? props.areasXL : getAreas(props));
const getAreasLG = (props: IGridStyledProps) => (props.areasLG ? props.areasLG : getAreasXL(props));
const getAreasMD = (props: IGridStyledProps) => (props.areasMD ? props.areasMD : getAreasLG(props));
const getAreasSM = (props: IGridStyledProps) => (props.areasSM ? props.areasSM : getAreasMD(props));
const getAreasXS = (props: IGridStyledProps) => (props.areasXS ? props.areasXS : getAreasSM(props));

/** Columns */
const getColumns = (props: IGridStyledProps) => (props.columns ? props.columns : 'none');
const getColumnsXL = (props: IGridStyledProps) => (props.columnsXL ? props.columnsXL : getColumns(props));
const getColumnsLG = (props: IGridStyledProps) => (props.columnsLG ? props.columnsLG : getColumnsXL(props));
const getColumnsMD = (props: IGridStyledProps) => (props.columnsMD ? props.columnsMD : getColumnsLG(props));
const getColumnsSM = (props: IGridStyledProps) => (props.columnsSM ? props.columnsSM : getColumnsMD(props));
const getColumnsXS = (props: IGridStyledProps) => (props.columnsXS ? props.columnsXS : getColumnsSM(props));

/** Rows */
const getRows = (props: IGridStyledProps) => (props.rows ? props.rows : 'none');
const getRowsXL = (props: IGridStyledProps) => (props.rowsXL ? props.rowsXL : getRows(props));
const getRowsLG = (props: IGridStyledProps) => (props.rowsLG ? props.rowsLG : getRowsXL(props));
const getRowsMD = (props: IGridStyledProps) => (props.rowsMD ? props.rowsMD : getRowsLG(props));
const getRowsSM = (props: IGridStyledProps) => (props.rowsSM ? props.rowsSM : getRowsMD(props));
const getRowsXS = (props: IGridStyledProps) => (props.rowsXS ? props.rowsXS : getRowsSM(props));

const getGridTemplateValues = (props: IGridStyledProps, scale: WidthScale) => {
    switch (scale) {
        case WidthScale.XS:
            return { areas: getAreasXS(props), columns: getColumnsXS(props), rows: getRowsXS(props) };
        case WidthScale.SM:
            return { areas: getAreasSM(props), columns: getColumnsSM(props), rows: getRowsSM(props) };
        case WidthScale.MD:
            return { areas: getAreasMD(props), columns: getColumnsMD(props), rows: getRowsMD(props) };
        case WidthScale.LG:
            return { areas: getAreasLG(props), columns: getColumnsLG(props), rows: getRowsLG(props) };
        case WidthScale.XL:
            return { areas: getAreasXL(props), columns: getColumnsXL(props), rows: getRowsXL(props) };
        default:
            return { areas: getAreas(props), columns: getColumns(props), rows: getRows(props) };
    }
};

const getGridTemplateStyle = (props: IGridStyledProps, scale?: WidthScale) => {
    const value = getGridTemplateValues(props, scale);

    if (props.areas) {
        return css`
            grid-template-areas: ${value.areas};
        `;
    }
    return css`
        grid-template-columns: ${value.columns};
        grid-template-rows: ${Number(value.rows) > 0 ? `repeat(${value.rows}, auto)` : value.rows};
    `;
};

const GridStyled = styled.div<IGridStyledProps>`
    position: relative;
    display: grid;
    grid-auto-columns: minmax(min-content, 1fr);
    ${(props) => getGridTemplateStyle(props)};
    grid-auto-flow: ${(props) => props.flow};
    grid-gap: ${(props) => props.gap};
    justify-items: ${(props) => props.justify};
    align-items: ${(props) => props.align};

    @media (min-width: 1800px) {
        ${(props) => getGridTemplateStyle(props, WidthScale.XL)}
    }

    @media (max-width: 1799px) {
        ${(props) => getGridTemplateStyle(props, WidthScale.LG)}
    }

    @media (max-width: 1199px) {
        ${(props) => getGridTemplateStyle(props, WidthScale.MD)}
    }

    @media (max-width: 899px) {
        ${(props) => getGridTemplateStyle(props, WidthScale.SM)}
    }

    @media (max-width: 599px) {
        ${(props) => getGridTemplateStyle(props, WidthScale.XS)}
    }
`;

export default GridStyled;
