import React, { ReactNode } from 'react';
import GridStyled, { IGridStyledProps } from './Grid.style';

export interface IGridProps extends IGridStyledProps {
    /** Content of the grid */
    children: ReactNode;
}

const Grid = (props: IGridProps) => {
    const { children, ...rest } = props;
    return <GridStyled {...rest}>{children}</GridStyled>;
};

Grid.defaultProps = {
    align: 'unset',
    columns: 'none',
    gap: '0',
    justify: 'unset',
    rows: 'none',
    flow: 'row',
};

export default Grid;
