import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import Text from '../Text';

let component: RenderResult;

describe('Text components tests', () => {
    beforeEach(() => {
        component = render(<Text>Hi There</Text>);
    });

    it('base properties were not changed', () => {
        expect(Text.defaultProps.align).toBe('left');
        expect(Text.defaultProps.delete).toBe(false);
        expect(Text.defaultProps.ellipsis).toBe(true);
        expect(Text.defaultProps.kind).toBe('text');
        expect(Text.defaultProps.size).toBe('medium');
        expect(Text.defaultProps.strong).toBe(false);
        expect(Text.defaultProps.underline).toBe(false);
    });

    it('renders correctly', () => {
        expect(component).toMatchSnapshot();
    });
});
