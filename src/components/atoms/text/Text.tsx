import React, { CSSProperties } from 'react';
import StyledText from './Text.style';
import { Kind, Size } from '../../../shared/types/Types';

export interface TextProps {
    /** Text alignment can be set as left, center, right or omitted (meaning left) */
    align?: 'left' | 'center' | 'right';
    /** Animate text on show */
    animated?: boolean;
    /** Column content */
    children: string;
    /** Deleted line style */
    deletedLine?: boolean;
    /** Display ellipsis when overflow. Can config rows and expandable by using object */
    ellipsis?: boolean;
    /** Text type can be set as danger, primary, secondary, success, warning, white or omitted (meaning text) */
    kind?: Kind | 'text' | 'white';
    /** Mark test as required insert a '*' at end of text */
    required?: boolean;
    /** Text size can be set as tiny, small, Large or omitted (meaning normal) */
    size?: 'tiny' | Size;
    /** Custom css style */
    style?: CSSProperties;
    /** Bold style */
    strong?: boolean;
    /** Text to show when hovering */
    title?: string;
    /** Underline style */
    underline?: boolean;
}

const Text = ({
    align,
    animated,
    ellipsis,
    deletedLine,
    kind,
    required,
    size,
    style,
    strong,
    underline,
    title,
    children,
}: TextProps) => (
    <>
        <StyledText
            {...{ align, animated, ellipsis, kind, required, size, strong, underline, title, deletedLine, style }}
        >
            {children}
        </StyledText>
    </>
);

Text.defaultProps = {
    align: 'left',
    delete: false,
    ellipsis: true,
    kind: 'text',
    size: 'medium',
    strong: false,
    underline: false,
};

export default Text;
