import styled, { css, keyframes } from 'styled-components';
import { typography, colors } from '../../../styles/theme';

export interface IStyledText {
    align?: string;
    animated?: boolean;
    deletedLine?: boolean;
    ellipsis?: boolean;
    kind?: string;
    required?: boolean;
    size?: string;
    strong?: boolean;
    underline?: boolean;
}

const sizes = {
    tiny: css`
        font-size: ${typography.fontXS};
    `,
    small: css`
        font-size: ${typography.fontSM};
    `,
    normal: css`
        font-size: ${typography.fontMD};
    `,
    large: css`
        font-size: ${typography.fontLG};
    `,
};

const fadeIn = () => keyframes`
    0% {
        opacity: 0;
    }
    100% {
        opacity: 1;
    }
`;

const StyledText = styled.span<IStyledText>`
    font-family: ${typography.fontFamily};
    ${({ size }) => sizes[size]};
    font-weight: ${(props) => (props.strong ? 600 : 400)};
    text-decoration: ${(props) => {
        if (props.deletedLine) {
            return 'line-through';
        }
        if (props.underline) {
            return 'underline';
        }
        return null;
    }};
    white-space: ${(props) => (props.ellipsis ? 'nowrap' : null)};
    overflow: ${(props) => (props.ellipsis ? 'hidden' : null)};
    text-align: ${(props) => props.align};
    text-overflow: ${(props) => (props.ellipsis ? 'ellipsis' : null)};
    color: ${({ kind }) => colors[kind]};

    ${(props) =>
        props.required
            ? css`
                  :after {
                      content: ' *';
                      color: ${colors.danger};
                  }
              `
            : null};

    ${(props) =>
        props.animated
            ? css`
                  animation: ${fadeIn} 0.3s ease-in;
              `
            : null};
`;

export default StyledText;
