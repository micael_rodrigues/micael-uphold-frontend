import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import Alert from '../Alert';

let component: RenderResult;

describe('Alert component tests', () => {
    beforeEach(() => {
        component = render(<Alert message='Some information' />);
    });

    it('base properties were not changed', () => {
        expect(Alert.defaultProps.kind).toBe('info');
        expect(Alert.defaultProps.size).toBe('medium');
    });

    it('renders correctly', () => {
        expect(component).toMatchSnapshot();
    });

    it('contains alert message', () => {
        expect(component.getByText('Some information')).toBeInTheDocument();
    });

    it('contains svg', () => {
        expect(component.container.querySelector('svg')).toBeInTheDocument();
    });
});
