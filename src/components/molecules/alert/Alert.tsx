import React from 'react';
import { colors } from '../../../styles/theme';
import Box from '../../atoms/box/Box';
import Text from '../../atoms/text/Text';
import { Size } from '../../../shared/types/Types';

interface AlertProps {
    kind?: 'warning' | 'danger' | 'info';
    size?: Size;
    message: string;
}

const sizes = {
    small: '20px',
    medium: '30px',
    large: '45px',
};

const Alert = ({ kind, message, size }: AlertProps) => {
    return (
        <Box>
            <div style={{ width: sizes[size], height: sizes[size] }}>
                <svg xmlns='http://www.w3.org/2000/svg' width='100%' height='100%' viewBox='0 0 24 24'>
                    <path
                        fill={colors[kind]}
                        d='M12 0c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-1 5h2v10h-2v-10zm1 14.25c-.69 0-1.25-.56-1.25-1.25s.56-1.25 1.25-1.25 1.25.56 1.25 1.25-.56 1.25-1.25 1.25z'
                    />
                </svg>
            </div>
            <div>
                <Text kind={kind}>{message}</Text>
            </div>
        </Box>
    );
};

Alert.defaultProps = {
    kind: 'info',
    size: 'medium',
};

export default Alert;
