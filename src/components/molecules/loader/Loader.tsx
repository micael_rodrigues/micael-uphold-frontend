import React from 'react';
import { Kind, Size } from '../../../shared/types/Types';
import Box from '../../atoms/box/Box';
import Spinner from '../../atoms/spinner/Spinner';

interface LoaderProps {
    message?: string;
    kind?: Kind;
    size?: Size;
}

const sizes = {
    small: '20px',
    medium: '30px',
    large: '45px',
};

const Loader = ({ message, size, kind }: LoaderProps) => (
    <Box>
        <div style={{ width: sizes[size], height: sizes[size] }}>
            <Spinner kind={kind} />
        </div>
        <div>{message}</div>
    </Box>
);

Loader.defaultProps = {
    kind: 'default',
    message: 'Loading...',
    size: 'medium',
};

export default Loader;
