import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import Loader from '../Loader';

let component: RenderResult;

describe('Loader component tests', () => {
    beforeEach(() => {
        component = render(<Loader message='Loading.' />);
    });

    it('base properties were not changed', () => {
        expect(Loader.defaultProps.kind).toBe('default');
        expect(Loader.defaultProps.message).toBe('Loading...');
        expect(Loader.defaultProps.size).toBe('medium');
    });

    it('renders correctly', () => {
        expect(component).toMatchSnapshot();
    });

    it('contains spinner svg', () => {
        expect(component.container.querySelector('svg')).toBeInTheDocument();
    });
});
