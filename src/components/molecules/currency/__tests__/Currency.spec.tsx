import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import Currency from '../Currency';

let component: RenderResult;

describe('Currency components tests', () => {
    beforeEach(() => {
        component = render(<Currency iso3='USD' />);
    });

    it('base properties were not changed', () => {
        expect(Currency.defaultProps.iconPosition).toBe('right');
    });

    it('renders correctly', () => {
        expect(component).toMatchSnapshot();
    });

    it('contains message and icon with the defined ISO3', () => {
        expect(component.container.querySelector('svg > use')).toBeInTheDocument();
    });
});
