import styled from 'styled-components';

const CurrencyStyled = styled.div`
    display: flex;
    align-items: center;
    * {
        padding-left: 0.2rem;
    }
`;

export default CurrencyStyled;
