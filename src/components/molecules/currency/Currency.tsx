import React from 'react';
import Text from '../../atoms/text/Text';
import CurrencyIcon from './CurrencyIcon';
import CurrencyStyled from './Currency.style';

interface CurrencyProps {
    /**
     * Currency iso3 code
     */
    iso3: string;
    /**
     * Position of the in relation to text
     */
    iconPosition?: 'left' | 'right';
}

const Currency = ({ iso3, iconPosition }: CurrencyProps) =>
    iconPosition === 'right' ? (
        <CurrencyStyled>
            <Text>{iso3}</Text> <CurrencyIcon iso3={iso3} />
        </CurrencyStyled>
    ) : (
        <CurrencyStyled>
            <CurrencyIcon iso3={iso3} /> <Text>{iso3}</Text>
        </CurrencyStyled>
    );

Currency.defaultProps = {
    iconPosition: 'right',
};

export default Currency;
