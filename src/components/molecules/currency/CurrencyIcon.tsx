import React from 'react';
import sprite from './currencies_sprite.svg';
import { Size } from '../../../shared/types/Types';

interface CurrencyIconProps {
    iso3: string;
    size?: Size;
}

const sizes = {
    small: {
        height: '16px',
        width: '16px',
    },
    medium: {
        height: '24px',
        width: '24px',
    },
    large: {
        height: '32px',
        width: '32px',
    },
};

const AVAILABLE_IDS = [
    'AED',
    'ARS',
    'AUD',
    'BAT',
    'BCH',
    'BRL',
    'BTC',
    'BTG',
    'CAD',
    'CHF',
    'CNY',
    'DASH',
    'DKK',
    'ETH',
    'EUR',
    'GBP',
    'HKD',
    'ILS',
    'INR',
    'JPY',
    'KES',
    'LTC',
    'MXN',
    'NOK',
    'NZD',
    'PHP',
    'PLN',
    'RVR',
    'SEK',
    'SGD',
    'UAE',
    'USD',
    'VOX',
    'XAG',
    'XAU',
    'XPD',
    'XPT',
    'XRP',
];

const CurrencyIcon = ({ iso3, size }: CurrencyIconProps) => {
    const id = AVAILABLE_IDS.includes(iso3) ? `${sprite}#${iso3}` : `${sprite}#Crypto`;
    const s = sizes[size];
    return (
        <svg width={s.width} height={s.height}>
            <use xlinkHref={id} />
        </svg>
    );
};

CurrencyIcon.defaultProps = {
    size: 'medium',
};

export default CurrencyIcon;
