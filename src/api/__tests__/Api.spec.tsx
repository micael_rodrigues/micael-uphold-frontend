import Api from '../Api';

let api: Api;

describe('Api', () => {
    beforeAll(() => {
        // handleChange = jest.fn();
        api = Api.getInstance();
    });
    it('Is a Singleton', () => {
        //  new Api()
        const other = Api.getInstance();
        expect(api === other).toBe(true);
    });

    it('Loads Currencies Map', async () => {
        const res = await Api.getInstance().getCurrencies();
        expect(res).not.toBeUndefined();
        expect(res.length).toBeGreaterThan(0);
    });
    /*
    it('Fetches Tickers for single currency ', async () => {
        const res = await Api.getInstance().getTicker('USD');
        expect(res).not.toBeUndefined();
    });
    */
});
