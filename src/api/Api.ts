export interface Ticker {
    ask: string;
    bid: string;
    currency: string;
    pair: string;
}

interface ErrorResponse {
    code: string;
    error: string;
}

export interface Currency {
    iso3: string;
}
type TickerMap = { [key: string]: Ticker[] };

export default class Api {
    private static instance: Api;

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    private constructor() {}

    static getInstance(): Api {
        Api.instance = Api.instance || new Api();
        return Api.instance;
    }

    /**
     * Check if response returned by a request is an error
     * @param object
     */
    static isErrorResponse(object: any): object is ErrorResponse {
        return 'code' in object;
    }

    private tickers: TickerMap;

    private currencies: string[] = [];

    /** Dummy method to simulate fetching currencies */
    private fetchAllTickers = async (): Promise<Ticker[]> => {
        return ((await (await import(`./data.json`)).default) as Ticker[]) || [];
    };

    private fetchCurrencyTickers = async (currency: string): Promise<Ticker[]> => {
        const response = await fetch(`/ticker/${currency}`);
        const tickers = (await response.json()) as Ticker[] | ErrorResponse;
        if (Api.isErrorResponse(tickers)) {
            return [];
        }
        /** Return a promise to simulate some delay */
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(
                    tickers
                        .filter((t) => t.currency !== currency && t.pair.startsWith(currency))
                        .map((t) => ({ ...t, ask: t.ask })),
                );
            }, 2000 * Math.random());
        });
    };

    getCurrencies = async (): Promise<string[] | undefined> => {
        if (this.currencies.length) {
            // currencies were loaded, don't load again
            return this.currencies;
        }
        const allTickers = await this.fetchAllTickers();
        if (!allTickers.length) {
            return undefined;
        }

        this.currencies = Array.from(new Set(allTickers.map((t) => t.currency)));
        return this.currencies;
    };

    getTicker = async (currency: string) => {
        if (!this.tickers) {
            this.tickers = {};
        }
        try {
            const newData = await this.fetchCurrencyTickers(currency);
            if (newData.length) {
                // TODO: Consider using local storage (even though this kind of data is volatile)
                this.tickers[currency] = newData;
            }
        } catch (e) {
            // Data for currency already exists, return it
            if (this.tickers[currency]) {
                return this.tickers[currency];
            }
            throw e;
        }

        // return previous data if nothing was returned from server or error ocurred
        return this.tickers[currency];
    };
}

export function fetchTickerData(currency: string) {
    const tickersPromise = Api.getInstance().getTicker(currency);
    const currenciesPromise = Api.getInstance().getCurrencies();
    return {
        currencies: wrapPromise(currenciesPromise),
        exchangeRates: wrapPromise(tickersPromise),
    };
}

// Suspense integrations like Relay implement
// a contract like this to integrate with React.
// Real implementations can be significantly more complex.
// Don't copy-paste this into your project!
function wrapPromise(promise) {
    let status = 'pending';
    let result;
    const suspender = promise.then(
        (r) => {
            status = 'success';
            result = r;
        },
        (e) => {
            status = 'error';
            result = e;
        },
    );
    return {
        read() {
            if (status === 'pending') {
                throw suspender;
            } else if (status === 'error') {
                throw result;
            }
            return result;
        },
    };
}
