# Exchanges Viewer

This project is a responsive converter between cryptocurrencies that uses [Uphold Tickers API](https://uphold.com/en/developer/api/documentation/#tickers).

## Demo

You can see a live demo at:

- [https://micael-frontend-test.netlify.app/](https://micael-frontend-test.netlify.app/)

## Installation and Usage

### Install

Download or clone repo to your project folder

```sh
npm install
```

### Usage

- `npm start` - Start development server in watch mode with source maps, opens [http://localhost:8888](http://localhost:8888). (Config can be changed in `.env` file)
- `npm test` - Runs [jest](https://github.com/facebook/jest) / [testing-library](https://github.com/testing-library/react-testing-library) tests
- `npm run format` - Formats `scr` files with [prettier](https://prettier.io/)
- `npm run lint` - Runs [ESLint](https://eslint.org/) on `scr` folder
- `npm run build` - `build` folder will include all the needed files for deploy.
- `npm run isready` - Runs the 3 previous scripts sequentially

## Technologies / Methodologies

The project uses a number of technologies to work properly:

- [React](https://reactjs.org/) - To build UI components. Use experimental [Suspense](https://reactjs.org/docs/concurrent-mode-suspense.html) / [Lazy](https://reactjs.org/docs/code-splitting.html#reactlazy) and [Error Boundary](https://reactjs.org/docs/error-boundaries.html) to improve page loading and error handling experience.
- [React Testing Library](https://testing-library.com/docs/react-testing-library/intro) for components testing
- [Typescript](https://www.typescriptlang.org/) - For type checking and compilation to plain JavaScript
- [StyledComponents](https://styled-components.com/) - For theming and styling combining ES6 and CSS (It also uses [CSS Grid](https://www.w3schools.com/css/css_grid.asp) and [CSS Flexbox](https://www.w3schools.com/css/css3_flexbox.asp) layout systems)
- [Scss](https://sass-lang.com/documentation/syntax#scss) - To global colors and fonts variables
- [SVG](https://www.w3.org/Graphics/SVG/) - For scalable icons. For currency icons a sprite is used, taking advantage of SVG symbols.

It was also developed using [Atomic Design](https://bradfrost.com/blog/post/atomic-web-design/) approach.
